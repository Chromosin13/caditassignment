# CADITAssignment

This repository is intended as a submission repo of coding assessment process

## Tech Stack
- Use 'localhost:4000' to renders browser
- Use Bootstrap 5 as front-end library
- Nodemon dependencies to create local server
- Node.js with MVC architecture pattern 
- express.js to simplify routing development

## Routing Structure
```text
    - localhost:4000/
        - assignment/
            - 1/ -> Render assignment1 working project
            - 2/ -> Render assignment2 working project
            - 3/ -> Render assignment3 working project

```

## Endpoint Access
- Assignment1 Work -> [Assignment1](http://localhost:4000/assignment/1)
- Assignment2 Work -> [Assignment2](http://localhost:4000/assignment/2)
- Assignment3 Work -> [Assignment3](http://localhost:4000/assignment/3)

## Repo Structure
Here is an example of the structure in general terms:

```text
- (root)
  - controllers
    - assignmentOneController.js
    - assignmentTwoController.js
    - assignmentThreeContoller.js
  - publics
    - assignment3.js
    - helper.js
  - models
    - salaryDataModel.js
    - sensorDataModel.js
    - updatedSensorDataModel.js
  - views
    - partials
        - footer.ejs
        - header.ejs
        - navbar.ejs
    - assignment1.ejs
    - assignment3.ejs
  - routes
    -assignment.route.js

```
