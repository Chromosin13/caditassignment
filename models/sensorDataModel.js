const mongoose = require('mongoose');

// configure object schema of salary_data collection which contains sensor_data.json
const sensorSchema = new mongoose.Schema({
    array: [{
        temperature: Number,
        humidity: Number,
        roomArea: String,
        id: Number,
        timestamp: {
            type: mongoose.Schema.Types.Mixed
        }
    }]
})

module.exports = mongoose.model('sensor_data', sensorSchema, 'sensor_data');