const mongoose = require('mongoose');

// configure object schema of salary_data collection which contains salary_data.json
const salarySchema = new mongoose.Schema({
    array: [{
        salaryInIDR: Number,
        salaryInUSD: Number,
        id: Number
    }]
})

module.exports = mongoose.model('salary_data', salarySchema, 'salary_data');