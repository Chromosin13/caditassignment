const mongoose = require('mongoose');

// configure object schema of updated_salary_data collection which contains updated sensor_data.json
const updatedSensorSchema = new mongoose.Schema({
    array: [{
        temperature: {
            type: Number
        },
        humidity: {
            type: Number
        },
        roomArea: {
            type: String
        },
        id: {
            type: Number
        },
        timestamp: {
            type: mongoose.Schema.Types.Mixed
        }
    }]
})

module.exports = mongoose.model('updated_sensor_data', updatedSensorSchema, 'updated_sensor_data');