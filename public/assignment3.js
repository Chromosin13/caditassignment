// configure canvas tags id in assignment3.ejs file
let chart_id = {
    "roomArea1": {
        "temperature": "roomArea1TempChart",
        "humidity": "roomArea1HumidChart"
    },
    "roomArea2": {
        "temperature": "roomArea2TempChart",
        "humidity": "roomArea2HumidChart"
    },
    "roomArea3": {
        "temperature": "roomArea3TempChart",
        "humidity": "roomArea3HumidChart"
    }
}

// configure display metrics div tags id in assignment3.ejs file
let display_metrics = [
    "roomArea1TempChart", "roomArea1HumidChart",
    "roomArea2TempChart", "roomArea2HumidChart",
    "roomArea3TempChart", "roomArea3HumidChart"
]

// convert array of epochs into DateTime String
const convertEpochToTime = (input) => {
    const date = new Date(input * 1)
    return {
        "day": date.getDate(),
        "month": date.getMonth(),
        "year": date.getFullYear(),
        "date": date.toLocaleDateString()
    }
}

// getData fetch data from args
const getData = async (url) => {
    try {
        const response = await fetch(url);
        const value = await response.json();
        return value;
    } catch (error) {
        console.log(error)
    }
}

// addData push new data into the chart
const addData = (chart, label, data) => {
    chart.data.labels.push(label);
    chart.data.datasets.forEach((dataset, index) => {
        dataset.data.push(data[index]);
    });
    const unique = [...new Set(chart.data.labels)]
    chart.options.scales.x.ticks.maxTicksLimit = unique.length;
    chart.update();
}

// organizeData organizes input array to be displayed in a form ocf chart and on metrics dashboard
const organizeData = (array) => {
    const data = [];
    Object.entries(array.room).forEach(entry1 => {
        const [key1, value1] = entry1;
        Object.entries(value1).forEach(entry2 => {
            const [key2, value2] = entry2;
            addData(chart_id[key1][key2], convertEpochToTime(array.last_timestamp).date, value2);
            data.push(value2);
        })
    });
    return data;
}

// Holds configuration for Room Area 1 Temperature Chart
chart_id.roomArea1.temperature = new Chart("roomArea1-temperature", {
    type: 'line',
    data: {
        labels: [],
        datasets: [
            {
                label: 'Min',
                data: [],
                fill: false,
                borderColor: 'rgb(75, 192, 192)',
                tension: 0.1
            },
            {
                label: 'Max',
                data: [],
                fill: false,
                borderColor: 'rgb(0,102,102)',
                tension: 0.1
            },
            {
                label: 'Median',
                data: [],
                fill: false,
                borderColor: 'rgb(102,0,51)',
                tension: 0.1
            },
            {
                label: 'Average',
                data: [],
                fill: false,
                borderColor: 'rgb(255,230,102)',
                tension: 0.1
            },
        ]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            },
            x: {
                ticks: {
                    maxTicksLimit: 3
                }
            }
        }
    }
});

// Holds configuration for Room Area 1 Humidity Chart
chart_id.roomArea1.humidity = new Chart("roomArea1-humidity", {
    type: 'line',
    data: {
        labels: [],
        datasets: [
            {
                label: 'Min',
                data: [],
                fill: false,
                borderColor: 'rgb(75, 192, 192)',
                tension: 0.1
            },
            {
                label: 'Max',
                data: [],
                fill: false,
                borderColor: 'rgb(0,102,102)',
                tension: 0.1
            },
            {
                label: 'Median',
                data: [],
                fill: false,
                borderColor: 'rgb(102,0,51)',
                tension: 0.1
            },
            {
                label: 'Average',
                data: [],
                fill: false,
                borderColor: 'rgb(255,230,102)',
                tension: 0.1
            },
        ]
    }
});

// Holds configuration for Room Area 2 Temperature Chart
chart_id.roomArea2.temperature = new Chart("roomArea2-temperature", {
    type: 'line',
    data: {
        labels: [],
        datasets: [
            {
                label: 'Min',
                data: [],
                fill: false,
                borderColor: 'rgb(75, 192, 192)',
                tension: 0.1
            },
            {
                label: 'Max',
                data: [],
                fill: false,
                borderColor: 'rgb(0,102,102)',
                tension: 0.1
            },
            {
                label: 'Median',
                data: [],
                fill: false,
                borderColor: 'rgb(102,0,51)',
                tension: 0.1
            },
            {
                label: 'Average',
                data: [],
                fill: false,
                borderColor: 'rgb(255,230,102)',
                tension: 0.1
            },
        ]
    }
});

// Holds configuration for Room Area 2 Humidity Chart
chart_id.roomArea2.humidity = new Chart("roomArea2-humidity", {
    type: 'line',
    data: {
        labels: [],
        datasets: [
            {
                label: 'Min',
                data: [],
                fill: false,
                borderColor: 'rgb(75, 192, 192)',
                tension: 0.1
            },
            {
                label: 'Max',
                data: [],
                fill: false,
                borderColor: 'rgb(0,102,102)',
                tension: 0.1
            },
            {
                label: 'Median',
                data: [],
                fill: false,
                borderColor: 'rgb(102,0,51)',
                tension: 0.1
            },
            {
                label: 'Average',
                data: [],
                fill: false,
                borderColor: 'rgb(255,230,102)',
                tension: 0.1
            },
        ]
    }
});

// Holds configuration for Room Area 3 Temperature Chart
chart_id.roomArea3.temperature = new Chart("roomArea3-temperature", {
    type: 'line',
    data: {
        labels: [],
        datasets: [
            {
                label: 'Min',
                data: [],
                fill: false,
                borderColor: 'rgb(75, 192, 192)',
                tension: 0.1
            },
            {
                label: 'Max',
                data: [],
                fill: false,
                borderColor: 'rgb(0,102,102)',
                tension: 0.1
            },
            {
                label: 'Median',
                data: [],
                fill: false,
                borderColor: 'rgb(102,0,51)',
                tension: 0.1
            },
            {
                label: 'Average',
                data: [],
                fill: false,
                borderColor: 'rgb(255,230,102)',
                tension: 0.1
            },
        ]
    }
});

// Holds configuration for Room Area 3 Humidity Chart
chart_id.roomArea3.humidity = new Chart("roomArea3-humidity", {
    type: 'line',
    data: {
        labels: [],
        datasets: [
            {
                label: 'Min',
                data: [],
                fill: false,
                borderColor: 'rgb(75, 192, 192)',
                tension: 0.1
            },
            {
                label: 'Max',
                data: [],
                fill: false,
                borderColor: 'rgb(0,102,102)',
                tension: 0.1
            },
            {
                label: 'Median',
                data: [],
                fill: false,
                borderColor: 'rgb(102,0,51)',
                tension: 0.1
            },
            {
                label: 'Average',
                data: [],
                fill: false,
                borderColor: 'rgb(255,230,102)',
                tension: 0.1
            },
        ]
    }
});

let timer = null, interval = 2000;
// Start simulation process
$("#startButton").click(() => {
    if (timer !== null) return;
    timer = setInterval(() => {
        getData("http://localhost:3000/assignment/3/get_latest_data")
        .then(results => {
            const data = organizeData(results);
            displayDashboardValues(display_metrics, data);
        })
    }, interval);
})

// Stop simulation process
$("#stopButton").click(() => {
    clearInterval(timer);
    timer = null;
})

// displayMetrics and displayDashboardValues display temperature and humidity metrics (as an array) into selected tags in browser
const displayMetrics = (id, array) => {
    const id_selector = "." + id;
    $(id_selector).each((key, value) => {
        $(value).text(array[key])
    })
}

const displayDashboardValues = (array_id, array_data) => {
    array_id.forEach((value, index) => {
        displayMetrics(value, array_data[index]);
    })
}

// First initialized promise function use to render first data into the browser
getData("http://localhost:3000/assignment/3/current_latest_data")
.then(results => {
    const data = organizeData(results);
    displayDashboardValues(display_metrics, data);
})