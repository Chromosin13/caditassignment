// convert array of epochs into Date String
const convertToDate = (input) => {
    input.forEach((element, index) => {
        const date = new Date(element.timestamp * 1).toLocaleDateString();
        element.timestamp = date;
    });
    return input;
}

// group by object of array by args
const groupBy = (key) => {
    return function group(array) {
      return array.reduce((acc, obj) => {
        const property = obj[key];
        acc[property] = acc[property] || [];
        acc[property].push(obj);
        return acc;
      }, {});
    };
}

// calculate max, min, median, and average of temperature and humidity and return nested object
const calculateMetrics = (array) => {
    arr_temp = array.map(element => element.temperature);
    arr_humid = array.map(element => element.humidity);
    
    const data = {
        "temperature": {
            "min": parseFloat(Math.min(...arr_temp).toFixed(3)),
            "max": parseFloat(Math.max(...arr_temp).toFixed(3)),
            "median": parseFloat(findMedian(arr_temp).toFixed(3)),
            "avg": parseFloat((arr_temp.reduce((a,b) => a + b, 0)/arr_temp.length).toFixed(3))
        },
        "humidity": {
            "min": parseFloat(Math.min(...arr_humid).toFixed(3)),
            "max": parseFloat(Math.max(...arr_humid).toFixed(3)),
            "median": parseFloat(findMedian(arr_humid).toFixed(3)),
            "avg": parseFloat((arr_humid.reduce((a,b) => a + b, 0)/arr_humid.length).toFixed(3))
        },
    }
    return data;
}

// sort array and return the sorted array
const getSortedArray = (arr) => {
	return arr.slice().sort((a, b) => a - b);
}

// find median from an array which return median value
const findMedian = (array) => {
    let sortedArr = getSortedArray(array);
	let inputLength = array.length;
	let middleIndex = Math.floor(inputLength / 2);
	let oddLength = inputLength % 2 != 0;
	let median;
	if(oddLength) { // if array length is odd -> return element at middleIndex
		median = sortedArr[middleIndex];
	} else {
		median = (sortedArr[middleIndex] + sortedArr[middleIndex - 1]) / 2;
	}
	return median;
}

module.exports = { 
    convertToDate,
    groupBy,
    calculateMetrics
}