// use sensor_data collection that is existed on atlas mongodb cloud database
const updatedSensorModel = require('../models/updatedSensorDataModel');

// helper will call selected method in helper file
const helper = require('../public/helper')

// ID_ITERATION specify ID multiplication on adding new random data point
const ID_ITERATION = 3;

// RANDOM_TIME_PERIOD specify iteration timestamp (in epoch time) when adding new random data point
const RANDOM_TIME_PERIOD = 2000; // 2000 ms

const pipeline_1 = [
    {
        $unwind: "$array"
    },
    {
        $group: {
            _id: "$array.timestamp",
            items: { $push: "$array" }
        }
    },
    {
        $sort: { "_id": -1 }
    },
    {
        $limit: 1
    }
]

// fetchData fetches updated semsor data inside updated_sensor_data collection in atlas mongoDB cloud database
const fetchData = async () => {
    return await updatedSensorModel.findOne({}, "array -_id").exec()
}

// generateNewRandomNumber generates random number respected to its upper and lower limit
const generateNewRandomNumber = (min, max) => {
    return parseFloat((Math.random() * (max - min)) + min).toFixed(14)
}

// addDataPoint generates ready-to-update new random data point
const addDataPoint = (input) => {
    let array = []
    input.items.forEach((value, index) => {
        array.push({
            "temperature": generateNewRandomNumber(1, 100),
            "humidity": generateNewRandomNumber(1, 99),
            "roomArea": input["items"][index]["roomArea"],
            "id": input["items"][index]["id"] + ID_ITERATION,
            "timestamp": input["_id"] + RANDOM_TIME_PERIOD
        })
    });
    return array;
}

//  updateDataPoint add new data point into updated_sensor_data collection in atlas mongoDB
const updateDataPoint = async (input) => {
    const results = updatedSensorModel.updateOne(
        { _id: "62c3a58c05310ff21d62d530" },
        { $push: { array: [input[0], input[1], input[2]] } }
    )
    return results
}

// getLastValue gets the last object inside updated_sensor_data collection by timestamp 
const getLastValue = async () => {
    const results = updatedSensorModel.aggregate(pipeline_1)
                        .then(value => value)
    return results
}

// groupByRoom groups array of objects inside updated_sensor_data collection by roomArea property
// the function also calculate min, max, median, anc average of each temperature and humidity on each roomArea
// the function returns array of objects
const groupByRoom = (array) => {
    const groupByRoom = helper.groupBy('roomArea');
    const grouped_room = groupByRoom(array);
    
    Object.entries(grouped_room).forEach(entry => {
        const [key, value] = entry;
        const data = helper.calculateMetrics(value);
        grouped_room[key] = {
            "temperature": Object.values(data.temperature),
            "humidity": Object.values(data.humidity)
        };
    })
    return grouped_room;
}

// currentLatestDataPoint:
// USE  -> return grouped by room data of temperature and humidity's min, max, median, and average
//      -> as well as the last timestamp recorded (in epoch format)
// STEPS
// 1. get last object of updated_sensor_data collection
// 2. fetch all data from updated_sensor_data collection
// 3. convert to date (to string data type and in a form of date) fetched data from step 2
// 4. group by room converted timestamp data and calculate temperature and humidity's min, max, median, and average on each roomArea
const currentLatestDataPoint = async (req, res) => {
    const last_data_point = await getLastValue(); // 1
    const fetched_data = await fetchData(); // 2
    const converted_data = await helper.convertToDate(fetched_data.array); // 3
    const grouped_room_data = await groupByRoom(converted_data); // 4

    res.send({
        "last_timestamp": last_data_point[0]._id,
        "room": grouped_room_data
    })
}

// getLatestDataPoint:
// USE  -> return grouped by room data of temperature and humidity's min, max, median, and average
//      -> as well as the last timestamp recorded (in epoch format)
// STEPS
// 1. get last object of updated_sensor_data collection
// 2. generate new random value data point with same timetamp in 3 different roomArea (roomArea1, roomArea2, roomArea3)
// 3. add the data generated from point 2 into updated_sensor_data collection
// 4. get new last object of updated_sensor_data collection

// STEP 5 - 7 has the same functionality with currentLatestDataPoint steps
// 5. fetch all data from updated_sensor_data collection
// 6. convert to date (to string data type and in a form of date) fetched data from step 2
// 7. group by room converted timestamp data and calculate temperature and humidity's min, max, median, and average on each roomArea
const getLatestDataPoint = async (req, res) => {
    const last_data_point = await getLastValue(); // 1
    const add_data = addDataPoint(last_data_point[0]); // 2
    const update_status = await updateDataPoint(add_data); // 3
    const new_last_data_point = await getLastValue();// 4

    const fetched_data = await fetchData(); // 5
    const converted_data = await helper.convertToDate(fetched_data.array); // 6
    const grouped_room_data = await groupByRoom(converted_data); // 7

    res.send({
        "last_timestamp": new_last_data_point[0]._id,
        "room": grouped_room_data
    })
}

const assignment3 = (req, res) => {
    res.render("assignment3.ejs")
}

module.exports = { 
    getLatestDataPoint,
    currentLatestDataPoint,
    assignment3
}