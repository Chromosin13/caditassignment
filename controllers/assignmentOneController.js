// use salary.json file that is existed on atlas mongodb cloud database
// "salaryModel" represent schema used to store data
const salaryModel = require('../models/salaryDataModel');

// request module to fetch data from API endpoint
const request = require('request');

// fetchData1 use to fetch data API from currency API -> conversion rate between IDR to USD
const fetchData1 = new Promise((resolve, reject) => {
    request("https://free.currconv.com/api/v7/convert?q=IDR_USD&compact=ultra&apiKey=828e066e75192f50d99f", (error, response, body) => {
        if (!error && response.statusCode == 200) {
            currency_rate = JSON.parse(body);
            resolve(currency_rate);
        }
    })
})

// fetchData2 use to fetch additional information regarding each employee's personal information
const fetchData2 = new Promise((resolve, reject) => {
    request("http://jsonplaceholder.typicode.com/users", (error, response, body) => {
        if(!error && response.statusCode == 200) {
            users_profile = JSON.parse(body);
            resolve(users_profile);
        }
    })
})

// fetchData3 use to fetch data from "salaryModel" data collection in atlas mongoDB
const fetchData3 = new Promise((resolve, reject) => {
    const value = salaryModel.findOne({}, "array -_id").exec();
    resolve(value);
})

// compile all above three promises into one array in which will be executed in assignment1 function
const promises_assignment1 = [fetchData1, fetchData2, fetchData3];

// mergeSalaryCurrency function merges each employee's personal information with its
// salary as well as convert its salary from IDR to USD
const mergeSalaryCurrency = (input) => {
    input[1].forEach((value, index) => {
        if (input[1][index].id == input[2]["array"][index].id) {
            value.salaryInUSD = input[0].IDR_USD*input[2]["array"][index].salaryInIDR;
            input[1][index] = {
                ...value,
                "salaryInIDR": input[2]["array"][index].salaryInIDR
            }
        }
    })
    return input[1];
}

// assignment1 controller method does several action:
// 1. merge salary and personal data of each employees as well convert employee's salary to USD
// 2.1. render views file (i.e. assignment1.ejs) into the browser
// 2.2. while passing data related to this assignment (employee's personal information and salary)
const assignment1 = (req, res) => {
    Promise.all(promises_assignment1).then((value) => {
        const data = mergeSalaryCurrency(value)
        res.render('assignment1.ejs', { "array": data });
    })
}

module.exports = { assignment1 }