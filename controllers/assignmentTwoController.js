// use sensor_data collection that is existed on atlas mongodb cloud database
const sensorModel = require('../models/sensorDataModel');

// helper will call selected method in helper file
const helper = require('../public/helper')

// fetchData fetches semsor data inside sensor_data collection in atlas mongoDB cloud database
const fetchData = async () => {
    return await sensorModel.findOne({}, "array -_id").exec()
}

// groupByRoomAndDate groups fetched data from fetchData function into Day and Room
const groupByRoomAndDate = (array) => {
    const groupByDate = helper.groupBy('timestamp');
    const grouped_date =  groupByDate(array);
    
    Object.entries(grouped_date).forEach(entry => {
        const [key1, value1] = entry;
        const groupByRoom = helper.groupBy('roomArea');
        const grouped_room = groupByRoom(value1);
        Object.entries(grouped_room).forEach(entry => {
            const [key2, value2] = entry;
            grouped_room[key2] = helper.calculateMetrics(value2);
        })
        grouped_date[key1] = grouped_room;
    })
    return grouped_date;
}

// assignment2 controller method does several action:
// 1. fetch data from sensor_data collection using fetchData function
// 2. convert timestamp into string Date using helper->convertToDate function 
// 3. group by room and date converted timestamp data
// 4. send output in a form of json-formatted document into browser
const assignment2 = async (req, res) => {
    const fetched_data = await fetchData();
    const converted_data = await helper.convertToDate(fetched_data.array);
    const grouped_date_data = groupByRoomAndDate(converted_data)
    res.send(grouped_date_data);
}

module.exports = {
    assignment2
}