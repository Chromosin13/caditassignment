const express = require("express"); 
const app = express();
const path = require("path")
const dotenv = require("dotenv");
const mongoose = require("mongoose");

dotenv.config();

mongoose
    .connect(process.env.MONGO_PROD_URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .then(() => console.log("Database connected!"))
    .catch(err => console.log(err));

app.listen(process.env.SERVER_PORT, function () {
    console.log("Server listening at: " + process.env.SERVER_PORT)
});

app.set("view engine", "ejs");

app.use('/static', express.static(path.join(__dirname, 'public')))

app.use('/js', express.static(path.join(__dirname + '/node_modules/bootstrap/dist/js'))); // redirect bootstrap JS
app.use('/js', express.static(path.join(__dirname + '/node_modules/jquery/dist'))); // redirect JS jQuery
app.use('/css', express.static(path.join(__dirname + '/node_modules/bootstrap/dist/css'))); // redirect CSS bootstrap

app.use("/assignment", require("./routes/assignment.route"));
