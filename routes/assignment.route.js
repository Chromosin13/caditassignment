const express = require("express");
const router = express.Router();
const controllerOne = require('../controllers/assignmentOneController');
const controllerTwo = require('../controllers/assignmentTwoController');
const controllerThree = require('../controllers/assignmentThreeController');

router.get('/1', controllerOne.assignment1)
router.get('/2', controllerTwo.assignment2)
router.get('/3', controllerThree.assignment3)
router.get('/3/get_latest_data', controllerThree.getLatestDataPoint)
router.get('/3/current_latest_data', controllerThree.currentLatestDataPoint)

module.exports = router;